using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LogUnit
{
    public class ConsoleOutputSniffer
    {
        private readonly StringBuilder _sniffedText = new StringBuilder();
        public string SniffedText => _sniffedText.ToString();

        public void StartSniffing()
        {
            Console.SetOut(new StringWriter(_sniffedText));
        }

        public void StopSniffing()
        {
            Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
        }
    }
}