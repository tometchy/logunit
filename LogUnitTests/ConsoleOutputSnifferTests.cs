using System;
using AutoFixture;
using LogUnit;
using Xunit;

namespace LogUnitTests
{
    public class ConsoleOutputSnifferTests
    {
        private readonly ConsoleOutputSniffer _sniffer = new ConsoleOutputSniffer();
        private readonly Fixture _fixture = new Fixture();

        [Fact]
        public void GIVEN_not_started_sniffer_WHEN_few_console_writes_occurred_THEN_sniffed_text_is_empty()
        {
            Console.Write(_fixture.Create<string>());
            Assert.Equal(string.Empty, _sniffer.SniffedText);

            Console.Write(_fixture.Create<string>());
            Assert.Equal(string.Empty, _sniffer.SniffedText);

            Console.WriteLine(_fixture.Create<string>());
            Assert.Equal(string.Empty, _sniffer.SniffedText);

            Console.WriteLine(_fixture.Create<string>());
            Assert.Equal(string.Empty, _sniffer.SniffedText);
        }

        [Fact]
        public void GIVEN_started_sniffer_WHEN_few_console_writes_occurred_THEN_every_write_extends_sniffed_text()
        {
            _sniffer.StartSniffing();

            var firstWrittenText = _fixture.Create<string>();
            Console.Write(firstWrittenText);
            var expected = firstWrittenText;
            Assert.Equal(expected, _sniffer.SniffedText);

            var secondWrittenText = _fixture.Create<string>();
            Console.Write(secondWrittenText);
            expected = expected + secondWrittenText;
            Assert.Equal(expected, _sniffer.SniffedText);

            var thirdWrittenText = _fixture.Create<string>();
            Console.WriteLine(thirdWrittenText);
            expected = expected + thirdWrittenText + Environment.NewLine;
            Assert.Equal(expected, _sniffer.SniffedText);
        }

        [Fact]
        public void GIVEN_stopped_sniffer_which_was_started_once_but_dit_not_capture_anything_WHEN_console_write_occured_THEN_sniffed_text_is_still_empty()
        {
            _sniffer.StartSniffing();
            _sniffer.StopSniffing();

            Console.Write(_fixture.Create<string>());
            Assert.Equal(string.Empty, _sniffer.SniffedText);
        }
    }
}